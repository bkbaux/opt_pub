#!/bin/sh


cwd=$(pwd)

aux=$HOME/aux

[ -d "$aux/Docs" ] || mkdir -p $aux/Docs


rm -f $aux/opt
ln -s $cwd $aux/opt

rm -f ~/.vim/bundle
ln -s $cwd/dotvim_bundle ~/.vim/bundle


rm -f $aux/Docs/web
ln -s $cwd/web $aux/Docs/web
